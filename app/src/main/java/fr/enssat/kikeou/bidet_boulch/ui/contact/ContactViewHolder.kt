package fr.enssat.kikeou.bidet_boulch.ui.contact

import android.view.View
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.enssat.kikeou.bidet_boulch.R
import fr.enssat.kikeou.bidet_boulch.databinding.ListItemContactBinding

class ContactViewHolder private constructor(
    var binding: ListItemContactBinding
) : RecyclerView.ViewHolder(binding.root) {

    private var onDeleteButtonClickedListener: ((Int) -> Unit)? = null

    init {
        binding.contactDeleteBtn.setOnClickListener {
            onDeleteButtonClickedListener?.invoke(adapterPosition)
        }
    }

    fun bind(
        id: String,
        name: String?,
        desc: String?,
        photo: String?,
        deleteEnable: Boolean = false
    ) {
        binding.contactItemName.text = name
        binding.contactItemDesc.text = desc

        Glide.with(itemView.context)
            .load(photo)
            .placeholder(
                ContextCompat.getDrawable(
                    itemView.context,
                    R.drawable.ic_baseline_person_24
                )
            )
            .into(binding.contactItemPhoto)

        binding.card.setOnClickListener {
            val action = ContactFragmentDirections.actionNavigationContactsToNavigationUser(id)
            Navigation.findNavController(itemView).navigate(action)
        }

        binding.contactDeleteBtn.visibility = if (deleteEnable) View.VISIBLE else View.GONE
    }

    fun setOnDeleteButtonClicked(listener: (itemIndex: Int) -> Unit) {
        this.onDeleteButtonClickedListener = listener
    }

    companion object {
        fun create(binding: ListItemContactBinding): ContactViewHolder {
            return ContactViewHolder(binding)
        }
    }
}