package fr.enssat.kikeou.bidet_boulch.database.entities

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import fr.enssat.kikeou.bidet_boulch.json.ListLocationConverter

@JsonClass(generateAdapter = true)
data class Location(var day: Int, var value: String)

@JsonClass(generateAdapter = true)
data class AgendaWithUser(
    @Embedded
    val agenda: Agenda,

    @Relation(
        parentColumn = "userCreatorId",
        entityColumn = "id"
    )
    val user: User
)

@Entity(
    tableName = "agenda_table",
    indices = [Index(value = ["userCreatorId", "week"], unique = true)],
    foreignKeys = [
        ForeignKey(
            entity = User::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("userCreatorId"),
            onDelete = CASCADE
        )
    ]
)
@JsonClass(generateAdapter = true)
data class Agenda(
    val userCreatorId: String,

    @ColumnInfo(name = "week")
    @field:Json(name = "week")
    var week: Int,

    @TypeConverters(ListLocationConverter::class)
    @field:Json(name = "loc")
    var loc: List<Location>
) {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    @Transient
    private var id: Long = 0

    fun getId(): Long {
        return id
    }

    fun setId(value: Long) {
        id = value
    }
}