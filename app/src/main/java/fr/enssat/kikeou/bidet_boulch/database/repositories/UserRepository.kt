package fr.enssat.kikeou.bidet_boulch.database.repositories

import androidx.annotation.WorkerThread
import fr.enssat.kikeou.bidet_boulch.database.dao.UserDao
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import kotlinx.coroutines.flow.Flow

class UserRepository(private val dao: UserDao) {
    val notApplicationOwnerUsers = dao.getAlphabetizedUsers(false)

    @WorkerThread
    fun insert(user: User) {
        dao.insert(user)
    }

    @WorkerThread
    suspend fun update(user: User) {
        dao.update(user)
    }

    fun getUserById(id: String): Flow<User?> {
        return dao.getUserById(id)
    }

    @WorkerThread
    fun delete(user: User) {
        dao.delete(user)
    }
}