package fr.enssat.kikeou.bidet_boulch.ui.user_agenda

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.bidet_boulch.databinding.ListItemAgendaLocationBinding

class UserAgendaLocationViewHolder private constructor(
    var binding: ListItemAgendaLocationBinding
) : RecyclerView.ViewHolder(binding.root) {

    private var onEditButtonClickedListener: ((Int) -> Unit)? = null
    private var onDeleteButtonClickedListener: ((Int) -> Unit)? = null

    init {
        binding.agendaLocationEditBtn.setOnClickListener {
            onEditButtonClickedListener?.invoke(adapterPosition)
        }

        binding.agendaLocationDeleteBtn.setOnClickListener {
            onDeleteButtonClickedListener?.invoke(adapterPosition)
        }
    }

    fun bind(title: String?, location: String?, enable: Boolean = false) {
        binding.agendaItemTitle.text = title
        binding.agendaItemLocation.text = location

        binding.agendaLocationEditBtn.visibility = if (enable) View.VISIBLE else View.GONE
        binding.agendaLocationDeleteBtn.visibility = if (enable) View.VISIBLE else View.GONE
    }

    fun setOnEditButtonClicked(listener: (itemIndex: Int) -> Unit) {
        this.onEditButtonClickedListener = listener
    }

    fun setOnDeleteButtonClicked(listener: (itemIndex: Int) -> Unit) {
        this.onDeleteButtonClickedListener = listener
    }

    companion object {
        fun create(binding: ListItemAgendaLocationBinding): UserAgendaLocationViewHolder {
            return UserAgendaLocationViewHolder(binding)
        }
    }
}