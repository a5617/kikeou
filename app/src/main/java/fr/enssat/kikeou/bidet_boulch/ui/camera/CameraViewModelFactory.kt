package fr.enssat.kikeou.bidet_boulch.ui.camera

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.enssat.kikeou.bidet_boulch.database.repositories.AgendaRepository

class CameraViewModelFactory (private val repository: AgendaRepository) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CameraViewModel(repository) as T
    }
}