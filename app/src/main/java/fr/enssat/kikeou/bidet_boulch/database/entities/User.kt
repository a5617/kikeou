package fr.enssat.kikeou.bidet_boulch.database.entities

import androidx.room.*
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import fr.enssat.kikeou.bidet_boulch.json.ListContactConverter
import java.util.*

@JsonClass(generateAdapter = true)
data class UserWithAgendas(
    @Embedded
    val user: User,

    @Relation(
        parentColumn = "id",
        entityColumn = "userCreatorId"
    )
    val agendas: List<Agenda>
)

@JsonClass(generateAdapter = true)
data class Contact(var key: String, var value: String?)

@Entity(tableName = "user_table")
@JsonClass(generateAdapter = true)
data class User(
    @ColumnInfo(name = "id")
    @PrimaryKey
    @field:Json(name = "id")
    var id: String = UUID.randomUUID().toString(),

    @ColumnInfo(name = "name")
    @field:Json(name = "name")
    var name: String = "",

    @ColumnInfo(name = "photo")
    @field:Json(name = "photo")
    var photo: String = "",

    @TypeConverters(ListContactConverter::class)
    @field:Json(name = "contact")
    var contact: List<Contact> = emptyList(),

    @ColumnInfo(name = "isApplicationOwner")
    @Transient
    private var isApplicationOwner: Boolean = false
) {
    fun getIsApplicationOwner(): Boolean {
        return isApplicationOwner
    }

    fun setIsApplicationOwner(value: Boolean) {
        isApplicationOwner = value
    }
}