package fr.enssat.kikeou.bidet_boulch.database.repositories

import androidx.annotation.WorkerThread
import fr.enssat.kikeou.bidet_boulch.database.dao.AgendaDao
import fr.enssat.kikeou.bidet_boulch.database.entities.Agenda
import fr.enssat.kikeou.bidet_boulch.database.entities.AgendaWithUser
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import kotlinx.coroutines.flow.Flow

class AgendaRepository(private val dao: AgendaDao) {
    @WorkerThread
    fun insert(agenda: Agenda) {
        dao.insert(agenda)
    }

    @WorkerThread
    fun update(agenda: Agenda) {
        dao.update(agenda)
    }

    @WorkerThread
    fun saveAgendaWithUser(agendaWithUser: AgendaWithUser) {
        val user: User? = dao.getUserById(agendaWithUser.user.id)
        if (user == null) {
            dao.insert(agendaWithUser)
        } else {
            dao.update(agendaWithUser)
        }
    }

    fun getAgendaByUserId(userId: String): Flow<List<Agenda>> {
        return dao.getAgendaByUserId(userId)
    }

    fun getAgendaWithUserByAgendaId(agendaId: Long): Flow<AgendaWithUser> {
        return dao.getAgendaWithUserByAgendaId(agendaId)
    }
}