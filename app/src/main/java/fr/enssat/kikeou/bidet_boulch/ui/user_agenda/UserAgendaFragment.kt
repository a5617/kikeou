package fr.enssat.kikeou.bidet_boulch.ui.user_agenda

import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import ca.antonious.materialdaypicker.MaterialDayPicker
import fr.enssat.kikeou.bidet_boulch.database.KikeouDatabase
import fr.enssat.kikeou.bidet_boulch.database.repositories.AgendaRepository
import fr.enssat.kikeou.bidet_boulch.databinding.FragmentUserAgendaBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import fr.enssat.kikeou.bidet_boulch.R
import fr.enssat.kikeou.bidet_boulch.database.entities.AgendaWithUser
import fr.enssat.kikeou.bidet_boulch.database.entities.Location
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.databinding.DialogDayFormBinding
import fr.enssat.kikeou.bidet_boulch.databinding.DialogQrcodeBinding
import fr.enssat.kikeou.bidet_boulch.json.AgendaWithUserJsonAdapter
import fr.enssat.kikeou.bidet_boulch.managers.QrCodeManager
import fr.enssat.kikeou.bidet_boulch.ui.user.UserFragment

class UserAgendaFragment : Fragment() {
    private lateinit var binding: FragmentUserAgendaBinding
    private lateinit var viewModel: UserAgendaViewModel

    private lateinit var agendaLocationListAdapter: UserAgendaLocationListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)

        // Define attributes and variables
        agendaLocationListAdapter = UserAgendaLocationListAdapter()

        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouDatabase.getDatabase(requireContext(), scope)
        val repository = AgendaRepository(database.agendaDao())

        // Inflate view
        binding = FragmentUserAgendaBinding.inflate(layoutInflater)

        // Init the ViewModel
        viewModel = ViewModelProvider(
            this,
            UserAgendaViewModelFactory(repository)
        ).get(UserAgendaViewModel::class.java)

        // Get the user from UserFragment viewModel
        val parentFragment: UserFragment = parentFragment as UserFragment
        parentFragment.viewModel.user.observe(viewLifecycleOwner) { user ->
            if (user != null) {
                viewModel.currentUser = user
            } else {
                viewModel.currentUser = User()
                viewModel.currentUser!!.setIsApplicationOwner(true)
            }

            viewModel.currentUser?.let {
                agendaLocationListAdapter.areValueEditable = it.getIsApplicationOwner()
            }

            // React to user agendas update
            viewModel.getAgendas(viewModel.currentUser!!.id)
                .observe(viewLifecycleOwner) { agendas ->
                    viewModel.currentUserAgendas = agendas
                    viewModel.currentWeek.value?.let { updateCurrentAgenda(it) }
                }

            // React to week change
            viewModel.currentWeek
                .observe(viewLifecycleOwner) { weekNumber ->
                    updateWeekTitle()
                    updateCurrentAgenda(weekNumber)
                }
        }

        // Init the agenda recyclerview
        initAgendaLocationRecyclerview()

        // Init buttons
        initAddDayBtn()
        initQRCodeBtn()
        initPrevFilledWeekBtn()
        initPrevWeekBtn()
        initNextWeekBtn()
        initNextFilledWeekBtn()

        return binding.root
    }

    private fun initAgendaLocationRecyclerview() {
        binding.recyclerview.adapter = agendaLocationListAdapter
        binding.recyclerview.layoutManager = LinearLayoutManager(requireContext())

        agendaLocationListAdapter.setOnItemEditButtonClickListener {
            launchEditDayDialog(it)
        }

        agendaLocationListAdapter.setOnItemDeleteButtonClickListener {
            viewModel.deleteLocationInCurrentAgenda(it)
        }
    }

    private fun initNextFilledWeekBtn() {
        binding.nextFilledWeekBtn.isEnabled = viewModel.isThereFilledAgendaNext()
        binding.nextFilledWeekBtn.setOnClickListener { viewModel.increaseWeekToFirstFilled() }
    }

    private fun initNextWeekBtn() {
        binding.nextWeekBtn.setOnClickListener { viewModel.increaseWeek() }
    }

    private fun initPrevWeekBtn() {
        binding.prevWeekBtn.setOnClickListener { viewModel.decreaseWeek() }
    }

    private fun initPrevFilledWeekBtn() {
        binding.prevFilledWeekBtn.isEnabled = viewModel.isThereFilledAgendaPrev()
        binding.prevFilledWeekBtn.setOnClickListener { viewModel.decreaseWeekToFirstFilled() }
    }

    private fun initAddDayBtn() {
        binding.addDayBtn.setOnClickListener { launchAddDayDialog() }
    }

    private fun initQRCodeBtn() {
        binding.agendaToQrCodeBtn.setOnClickListener { launchQRCodeAlertDialog() }
    }

    /**
     * Update the week title in the view
     */
    private fun updateWeekTitle() {
        binding.weekTitle.text =
            resources.getString(R.string.user_tab_agenda_week_title, viewModel.currentWeek.value)
    }

    private fun updateCurrentAgenda(weekNumber: Int) {
        viewModel.currentAgenda =
            viewModel.currentUserAgendas?.firstOrNull { agenda -> agenda.week == weekNumber }

        // Set location recyclerview
        agendaLocationListAdapter.submitList(
            viewModel.currentAgenda?.loc?.sortedWith(compareBy { it.day })
                ?: emptyList()
        )

        // Set add day button visibility
        binding.addDayBtn.visibility =
            if (viewModel.currentAgenda?.loc?.count() ?: 0 < 5
                && viewModel.currentUser!!.getIsApplicationOwner()
            ) View.VISIBLE else View.GONE

        // Set QR Code button visibility
        binding.agendaToQrCodeBtn.visibility =
            if (viewModel.currentAgenda == null) View.GONE else View.VISIBLE

        // Set prev & next filled week button usability
        binding.prevFilledWeekBtn.isEnabled = viewModel.isThereFilledAgendaPrev()
        binding.nextFilledWeekBtn.isEnabled = viewModel.isThereFilledAgendaNext()
    }

    /**
     * Launch the dialog which permit to add a location in the selected week.
     */
    private fun launchAddDayDialog() {
        // Init dialog view
        val dialogBinding = DialogDayFormBinding.inflate(
            LayoutInflater.from(requireContext()),
            null,
            false
        )

        // Build dialog
        val materialAlertDialogBuilder = MaterialAlertDialogBuilder(requireContext())
        val dialog = materialAlertDialogBuilder
            .setView(dialogBinding.root)
            .setTitle(R.string.dialog_day_add_title)
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.add) { dialog, _ ->
                // Retrieve data from dialog
                val dayIndex: Int = (dialogBinding.dayPicker.selectedDays[0].ordinal).mod(
                    MaterialDayPicker.Weekday.values().count()
                )
                val dayLocation: String = dialogBinding.dayLocationField.editText?.text.toString()

                // Insert location in agenda
                viewModel.insertLocationInCurrentAgenda(Location(dayIndex, dayLocation))

                // Close dialog
                dialog.dismiss()
            }
            .show()

        // Initially disable the positive button
        dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false

        // Init the day picker
        dialogBinding.dayPicker.disableWeekends()
        viewModel.currentAgenda?.let {
            it.loc.forEach { loc ->
                dialogBinding.dayPicker.disableDay(MaterialDayPicker.Weekday[loc.day])
            }
        }

        // Handle events
        dialogBinding.dayPicker.setDaySelectionChangedListener { days ->
            dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled =
                days.count() == 1 && !TextUtils.isEmpty(dialogBinding.dayLocationField.editText?.text)
        }

        dialogBinding.dayLocationField.editText?.doAfterTextChanged { value ->
            dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled =
                dialogBinding.dayPicker.selectedDays.count() == 1 && !TextUtils.isEmpty(value)
        }
    }

    /**
     * Launch the dialog which permit to edit a selected location.
     *
     * @param location Location to edit.
     */
    private fun launchEditDayDialog(location: Location) {
        // Init dialog view
        val dialogBinding = DialogDayFormBinding.inflate(
            LayoutInflater.from(requireContext()),
            null,
            false
        )

        // Build dialog
        val materialAlertDialogBuilder = MaterialAlertDialogBuilder(requireContext())
        val dialog = materialAlertDialogBuilder
            .setView(dialogBinding.root)
            .setTitle(R.string.dialog_day_edit_title)
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.add) { dialog, _ ->
                // Retrieve data from dialog
                location.day = (dialogBinding.dayPicker.selectedDays[0].ordinal).mod(
                    MaterialDayPicker.Weekday.values().count()
                )
                location.value = dialogBinding.dayLocationField.editText?.text.toString()

                // Insert location in agenda
                viewModel.updateCurrentAgenda()

                // Close dialog
                dialog.dismiss()
            }
            .show()

        // Initially disable the positive button
        dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false

        // Init values

        //// Init the day picker
        dialogBinding.dayPicker.disableWeekends()
        dialogBinding.dayPicker.selectDay(MaterialDayPicker.Weekday[location.day])
        viewModel.currentAgenda?.let {
            it.loc.forEach { loc ->
                if (loc != location) {
                    dialogBinding.dayPicker.disableDay(MaterialDayPicker.Weekday[loc.day])
                }
            }
        }

        //// Init the location field
        dialogBinding.dayLocationField.editText?.setText(location.value)

        // Handle events
        dialogBinding.dayPicker.setDaySelectionChangedListener { days ->
            dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled =
                days.count() == 1 && !TextUtils.isEmpty(dialogBinding.dayLocationField.editText?.text)
        }

        dialogBinding.dayLocationField.editText?.doAfterTextChanged { value ->
            dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled =
                dialogBinding.dayPicker.selectedDays.count() == 1 && !TextUtils.isEmpty(value)
        }
    }

    /**
     * Launch the dialog which shows the QR Code of the selected week.
     */
    private fun launchQRCodeAlertDialog() {
        // Init dialog view
        val dialogBinding = DialogQrcodeBinding.inflate(
            LayoutInflater.from(requireContext()),
            null,
            false
        )

        // Generate agenda QR Code
        val liveData = viewModel.getCurrentAgendaWithUser()
        liveData?.observe(viewLifecycleOwner, object : Observer<AgendaWithUser> {
            override fun onChanged(agendaWithUser: AgendaWithUser) {
                val agendaJson = AgendaWithUserJsonAdapter.agendaWithUserToJson(agendaWithUser)
                Log.i("UserAgendaFragment", "GENERATED JSON : $agendaJson")

                val file = QrCodeManager().getQrCode(agendaJson)
                dialogBinding.qrcode.setImageURI(file.absolutePath.toUri())

                // Building the Alert dialog using materialAlertDialogBuilder instance
                val materialAlertDialogBuilder = MaterialAlertDialogBuilder(requireContext())
                materialAlertDialogBuilder
                    .setView(dialogBinding.root)
                    .setNegativeButton(R.string.close) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .show()

                // Observe once
                liveData.removeObserver(this)
            }
        })
    }

}