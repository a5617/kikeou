package fr.enssat.kikeou.bidet_boulch.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import fr.enssat.kikeou.bidet_boulch.database.dao.AgendaDao
import fr.enssat.kikeou.bidet_boulch.database.dao.UserDao
import fr.enssat.kikeou.bidet_boulch.database.entities.Agenda
import fr.enssat.kikeou.bidet_boulch.database.entities.Contact
import fr.enssat.kikeou.bidet_boulch.database.entities.Location
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.json.ListContactConverter
import fr.enssat.kikeou.bidet_boulch.json.ListLocationConverter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@TypeConverters(ListLocationConverter::class, ListContactConverter::class)
@Database(
    entities = [Agenda::class, User::class],
    version = 1,
    exportSchema = false
)
abstract class KikeouDatabase : RoomDatabase() {

    abstract fun agendaDao(): AgendaDao
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: KikeouDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): KikeouDatabase {
            synchronized(this) {
                var instance = INSTANCE

                // If instance is `null` make a new database instance.
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        KikeouDatabase::class.java,
                        "kikeou_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(KikeouDatabaseCallback(scope))
                        .build()
                    INSTANCE = instance
                }

                return instance
            }
        }

        private class KikeouDatabaseCallback(val scope: CoroutineScope) : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populate(database.agendaDao(), database.userDao())
                    }
                }
            }
        }

        /* populate db in coroutine*/
        suspend fun populate(agendaDao: AgendaDao, userDao: UserDao) {
            userDao.deleteAll()
            agendaDao.deleteAll()

            var contact = arrayListOf(Contact("mail", "guillaume.chatelet@orange.com"))
            var user = User(name = "guillaume chatelet", contact = contact)
            userDao.insert(user)

            var location = arrayListOf(Location(2, "en TT"), Location(5, "en TT"))
            var agenda = Agenda(user.id, 47, location)
            agendaDao.insert(agenda)

            contact = arrayListOf(Contact("mail", "pierre.crepieux@orange.com"))
            user = User(name = "pierre crepieux", contact = contact)
            userDao.insert(user)

            location = arrayListOf(Location(3, "en TT"), Location(4, "en TT"))
            agenda = Agenda(user.id, 47, location)
            agendaDao.insert(agenda)

            contact = arrayListOf(
                Contact("mail", "lambda.photo@gmail.com"),
                Contact("tel", "02 00 00 00 00")
            )
            user = User(
                name = "Lambda Photo",
                photo = "https://kazeistore.files.wordpress.com/2018/09/pile-face3.jpg",
                contact = contact
            )
            userDao.insert(user)

            location = arrayListOf(
                Location(3, "en TT"),
                Location(4, "en TT")
            )
            agenda = Agenda(user.id, 50, location)
            agendaDao.insert(agenda)

            location = arrayListOf(
                Location(1, "teletravail"),
                Location(3, "Off"),
                Location(5, "WF 036")
            )
            agenda = Agenda(user.id, 52, location)
            agendaDao.insert(agenda)
        }
    }
}