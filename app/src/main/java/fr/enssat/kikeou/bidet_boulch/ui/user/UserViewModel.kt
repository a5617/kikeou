package fr.enssat.kikeou.bidet_boulch.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.database.repositories.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class UserViewModel(
    private val userId: String,
    private val repository: UserRepository
) : ViewModel() {

    private val scope = CoroutineScope(SupervisorJob())

    var user: LiveData<User?> = repository.getUserById(userId).asLiveData()

    fun insertUser(usr: User) = scope.launch {
        repository.insert(usr)
    }
}