package fr.enssat.kikeou.bidet_boulch.ui.user_agenda

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.enssat.kikeou.bidet_boulch.database.entities.Location
import fr.enssat.kikeou.bidet_boulch.databinding.ListItemAgendaLocationBinding
import java.time.DayOfWeek
import java.time.format.TextStyle
import java.util.*

class UserAgendaLocationListAdapter :
    ListAdapter<Location, UserAgendaLocationViewHolder>(LocationDiff()) {

    var areValueEditable: Boolean = false

    private var onItemDeleteButtonClickListener: ((item: Location) -> Unit)? = null
    private var onItemEditButtonClickListener: ((item: Location) -> Unit)? = null

    fun setOnItemDeleteButtonClickListener(listener: (item: Location) -> Unit) {
        this.onItemDeleteButtonClickListener = listener
    }

    fun setOnItemEditButtonClickListener(listener: (item: Location) -> Unit) {
        this.onItemEditButtonClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UserAgendaLocationViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemAgendaLocationBinding.inflate(inflater, parent, false)
        val viewHolder = UserAgendaLocationViewHolder.create(binding)

        viewHolder.setOnEditButtonClicked { onItemEditButtonClickListener?.invoke(getItem(it)) }
        viewHolder.setOnDeleteButtonClicked { onItemDeleteButtonClickListener?.invoke(getItem(it)) }

        return viewHolder
    }

    override fun onBindViewHolder(holder: UserAgendaLocationViewHolder, position: Int) {
        val current: Location? = getItem(position)
        val title = current?.day?.let { dayIndexToString(it) }
        val location = current?.value
        holder.bind(title, location, areValueEditable)
    }

    internal class LocationDiff : DiffUtil.ItemCallback<Location>() {
        override fun areItemsTheSame(oldItem: Location, newItem: Location): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Location, newItem: Location): Boolean {
            return oldItem.day == newItem.day
        }
    }

    private fun dayIndexToString(index: Int): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            DayOfWeek
                .values()[(index - 1).mod(DayOfWeek.values().count())]
                .getDisplayName(
                    TextStyle.FULL,
                    Locale.FRENCH
                )
                .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
        } else {
            when (index) {
                1 -> "Lundi"
                2 -> "Mardi"
                3 -> "Mercredi"
                4 -> "Jeudi"
                5 -> "Vendredi"
                6 -> "Samedi"
                7 -> "Dimanche"
                else -> "Jour inconnu"
            }
        }
    }
}