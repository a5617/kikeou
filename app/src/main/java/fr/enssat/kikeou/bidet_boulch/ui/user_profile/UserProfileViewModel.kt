package fr.enssat.kikeou.bidet_boulch.ui.user_profile

import androidx.lifecycle.ViewModel
import fr.enssat.kikeou.bidet_boulch.database.entities.Contact
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.database.repositories.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class UserProfileViewModel(
    private val repository: UserRepository
) : ViewModel() {
    private val scope = CoroutineScope(SupervisorJob())

    lateinit var user: User
    var modifiedUser: User? = null

    fun insertContactInUser(newContact: Contact) = scope.launch {
        user.contact += newContact
        repository.update(user)
    }

    fun updateUser() = scope.launch {
        modifiedUser?.let { repository.update(it) }
    }

    fun deleteContactInUser(contact: Contact) = scope.launch {
        user.contact -= contact
        repository.update(user)
    }
}