package fr.enssat.kikeou.bidet_boulch.ui.user_profile

import android.app.Dialog
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import fr.enssat.kikeou.bidet_boulch.R
import fr.enssat.kikeou.bidet_boulch.database.KikeouDatabase
import fr.enssat.kikeou.bidet_boulch.database.entities.Contact
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.database.repositories.UserRepository
import fr.enssat.kikeou.bidet_boulch.databinding.DialogContactFormBinding
import fr.enssat.kikeou.bidet_boulch.databinding.FragmentUserProfileBinding
import fr.enssat.kikeou.bidet_boulch.ui.user.UserFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class UserProfileFragment : Fragment() {

    private lateinit var binding: FragmentUserProfileBinding
    private lateinit var viewModel: UserProfileViewModel

    private lateinit var contactListAdapter: UserProfileContactListAdapter
    private lateinit var materialAlertDialogBuilder: MaterialAlertDialogBuilder

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)

        // Define attributes and variables
        materialAlertDialogBuilder = MaterialAlertDialogBuilder(requireContext())

        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouDatabase.getDatabase(requireContext(), scope)
        val repository = UserRepository(database.userDao())

        // Inflate view
        binding = FragmentUserProfileBinding.inflate(inflater, container, false)

        // Init the ViewModel
        val viewModelFactory = UserProfileViewModelFactory(repository)
        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(UserProfileViewModel::class.java)

        // Get the user from UserFragment viewModel
        val parentFragment: UserFragment = parentFragment as UserFragment
        parentFragment.viewModel.user.observe(viewLifecycleOwner) { user ->
            val validUser: User =
                if (user != null) {
                    user
                } else {
                    val emptyUser = User()
                    emptyUser.setIsApplicationOwner(true)
                    emptyUser
                }

            updateUserData(validUser)
            viewModel.user = validUser
            viewModel.modifiedUser = validUser.copy()
        }

        initContactRecyclerview()
        initUserNameField()
        initUserPhotoField()
        initAddContactBtn()
        initSaveProfileBtn()

        return binding.root
    }

    private fun initContactRecyclerview() {
        contactListAdapter = UserProfileContactListAdapter()
        binding.recyclerviewContact.adapter = contactListAdapter
        binding.recyclerviewContact.layoutManager = LinearLayoutManager(requireContext())

        contactListAdapter.setOnItemEditButtonClickListener {
            launchEditContactDialog(it)
        }

        contactListAdapter.setOnItemDeleteButtonClickListener {
            viewModel.deleteContactInUser(it)
        }
    }

    private fun initUserNameField() {
        binding.nameField.editText?.doAfterTextChanged { value ->
            val parentFragment = parentFragment as UserFragment
            val currentUser = parentFragment.viewModel.user.value

            viewModel.modifiedUser?.name = value.toString()

            binding.saveProfileBtn.visibility =
                if (value.contentEquals(currentUser?.name)
                    && viewModel.modifiedUser?.photo.contentEquals(currentUser?.photo)
                ) View.GONE else View.VISIBLE
        }
    }

    private fun initUserPhotoField() {
        binding.photoField.editText?.doAfterTextChanged { value ->
            val parentFragment = parentFragment as UserFragment
            val currentUser = parentFragment.viewModel.user.value

            viewModel.modifiedUser?.photo = value.toString()

            binding.saveProfileBtn.visibility =
                if (value.contentEquals(currentUser?.photo)
                    && viewModel.modifiedUser?.name.contentEquals(currentUser?.name)
                ) View.GONE else View.VISIBLE
        }
    }

    private fun initAddContactBtn() {
        binding.addContactBtn.setOnClickListener {
            launchAddContactDialog()
        }
    }

    private fun initSaveProfileBtn() {
        binding.saveProfileBtn.setOnClickListener {
            viewModel.updateUser()
        }
    }

    private fun updateUserData(user: User) {
        // User is application owner
        val isUserApplicationOwner = user.getIsApplicationOwner()
        binding.nameField.isEnabled = isUserApplicationOwner
        binding.photoField.isEnabled = isUserApplicationOwner
        contactListAdapter.areValueEditable = isUserApplicationOwner

        binding.addContactBtn.visibility = if (!isUserApplicationOwner) View.GONE else View.VISIBLE

        // Manage nameField
        binding.nameField.editText?.setText(user.name)

        // Manage photoField
        binding.photoField.editText?.setText(user.photo)

        // Manage contactList
        contactListAdapter.submitList(user.contact)
    }

    /**
     * Display the dialog which permit to add a contact to the user.
     */
    private fun launchAddContactDialog() {
        // Init dialog view
        val dialogBinding = DialogContactFormBinding.inflate(
            LayoutInflater.from(requireContext()),
            null,
            false
        )

        // Build dialog
        val dialog = materialAlertDialogBuilder
            .setView(dialogBinding.root)
            .setTitle(R.string.dialog_contact_add_title)
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.add) { dialog, _ ->
                // Retrieve data from dialog
                val newKey = dialogBinding.contactKeyField.editText?.text.toString()
                val newValue = dialogBinding.contactValueField.editText?.text.toString()
                val newContact = Contact(newKey, newValue)

                // Insert contact in user
                viewModel.insertContactInUser(newContact)

                // Close dialog
                dialog.dismiss()
            }
            .show()

        // Initially disable the positive button
        dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false

        // Handle events
        dialogBinding.contactKeyField.editText?.doAfterTextChanged { value ->
            dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled =
                !TextUtils.isEmpty(
                    dialogBinding.contactValueField.editText?.text
                ) && !TextUtils.isEmpty(
                    value
                )
        }

        dialogBinding.contactValueField.editText?.doAfterTextChanged { value ->
            dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled =
                !TextUtils.isEmpty(
                    dialogBinding.contactKeyField.editText?.text
                ) && !TextUtils.isEmpty(
                    value
                )
        }
    }

    /**
     * Display the dialog which permit to edit a contact of the user.
     */
    private fun launchEditContactDialog(contact: Contact) {
        // Init dialog view
        val dialogBinding = DialogContactFormBinding.inflate(
            LayoutInflater.from(requireContext()),
            null,
            false
        )

        // Build dialog
        val dialog = materialAlertDialogBuilder
            .setView(dialogBinding.root)
            .setTitle(R.string.dialog_contact_edit_title)
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .setPositiveButton(R.string.apply) { dialog, _ ->
                // Retrieve data from dialog
                contact.key = dialogBinding.contactKeyField.editText?.text.toString()
                contact.value = dialogBinding.contactValueField.editText?.text.toString()

                // Insert contact in user
                viewModel.updateUser()

                // Close dialog
                dialog.dismiss()
            }
            .show()

        // Initially disable the positive button
        dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled = false

        // Init values
        dialogBinding.contactKeyField.editText?.setText(contact.key)
        dialogBinding.contactValueField.editText?.setText(contact.value)

        // Handle events
        dialogBinding.contactKeyField.editText?.doAfterTextChanged { value ->
            dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled =
                !TextUtils.isEmpty(
                    dialogBinding.contactValueField.editText?.text
                ) && !TextUtils.isEmpty(
                    value
                )
        }

        dialogBinding.contactValueField.editText?.doAfterTextChanged { value ->
            dialog.getButton(Dialog.BUTTON_POSITIVE).isEnabled =
                !TextUtils.isEmpty(
                    dialogBinding.contactKeyField.editText?.text
                ) && !TextUtils.isEmpty(
                    value
                )
        }
    }

}