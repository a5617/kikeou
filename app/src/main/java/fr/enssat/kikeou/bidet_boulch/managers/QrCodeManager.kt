package fr.enssat.kikeou.bidet_boulch.managers

import net.glxn.qrgen.android.QRCode

import net.glxn.qrgen.core.image.ImageType
import java.io.File


class QrCodeManager {

    fun getQrCode(json : String) : File {
        // override size and image type
        return QRCode.from(json).to(ImageType.PNG).withSize(512, 512).file()
    }

}