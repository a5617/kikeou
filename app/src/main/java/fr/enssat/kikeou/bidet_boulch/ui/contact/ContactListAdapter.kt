package fr.enssat.kikeou.bidet_boulch.ui.contact

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.enssat.kikeou.bidet_boulch.R
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.databinding.ListItemContactBinding
import java.util.*

class ContactListAdapter : RecyclerView.Adapter<ContactViewHolder>(), Filterable {

    private var userList: List<User?> = emptyList()

    private var filteredUserList: List<User?> = userList
    private var onItemDeleteButtonClickListener: ((item: User) -> Unit)? = null

    var areItemsDeletable: Boolean = false
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    fun setOnItemDeleteButtonClickListener(listener: (item: User) -> Unit) {
        this.onItemDeleteButtonClickListener = listener
    }

    fun submitList(list: List<User?>) {
        userList = list
        filteredUserList = userList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemContactBinding.inflate(inflater, parent, false)
        return ContactViewHolder.create(binding)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val current: User? = filteredUserList[position]

        val id = current?.id
        val name = current?.name
        val desc = holder.itemView.context.resources.getString(R.string.user_id, current?.id)
        val photo = current?.photo

        if (id != null) {
            holder.bind(id, name, desc, photo, areItemsDeletable)
        }

        holder.setOnDeleteButtonClicked { onItemDeleteButtonClickListener?.invoke(filteredUserList[it]!!) }
    }

    override fun getItemCount(): Int {
        return filteredUserList.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val searchString = charSequence.toString()
                filteredUserList = if (searchString.isEmpty()) {
                    userList
                } else {
                    val tempFilteredList: MutableList<User?> = emptyList<User>().toMutableList()
                    for (user in userList) {

                        // Search for user name
                        if (user?.name?.lowercase(Locale.getDefault())
                                ?.contains(searchString) == true
                        ) {
                            tempFilteredList += user
                        }
                    }
                    tempFilteredList
                }
                val filterResults = FilterResults()
                filterResults.values = filteredUserList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                (filterResults.values as List<User?>).also { userList ->
                    filteredUserList = userList
                }
                notifyDataSetChanged()
            }
        }
    }
}