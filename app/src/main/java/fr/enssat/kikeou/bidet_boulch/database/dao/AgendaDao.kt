package fr.enssat.kikeou.bidet_boulch.database.dao

import androidx.room.*
import fr.enssat.kikeou.bidet_boulch.database.entities.Agenda
import fr.enssat.kikeou.bidet_boulch.database.entities.AgendaWithUser
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import kotlinx.coroutines.flow.Flow

@Dao
interface AgendaDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(agenda: Agenda)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Transaction
    fun insert(agendaWithUser: AgendaWithUser) {
        insert(agendaWithUser.user)
        insert(agendaWithUser.agenda)
    }

    @Update
    fun update(agenda: Agenda)

    @Update
    fun update(user: User)

    @Transaction
    fun update(agendaWithUser: AgendaWithUser) {
        update(agendaWithUser.user)
        insert(agendaWithUser.agenda)
    }

    @Query("DELETE FROM agenda_table")
    fun deleteAll()

    @Query("SELECT * FROM agenda_table WHERE agenda_table.userCreatorId = :userId ORDER BY week ASC")
    fun getAgendaByUserId(userId: String): Flow<List<Agenda>>

    @Transaction
    @Query("SELECT * FROM agenda_table WHERE id = :id")
    fun getAgendaWithUserByAgendaId(id: Long): Flow<AgendaWithUser>

    @Query("SELECT * FROM user_table WHERE id = :id")
    fun getUserById(id: String): User?
}