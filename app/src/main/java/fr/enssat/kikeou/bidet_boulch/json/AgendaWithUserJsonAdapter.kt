package fr.enssat.kikeou.bidet_boulch.json

import com.squareup.moshi.*
import fr.enssat.kikeou.bidet_boulch.database.entities.*

class AgendaWithUserJsonAdapter {

    @JsonClass(generateAdapter = true)
    data class AgendaWithUserJson(
        @field:Json(name = "id")
        val id: String,

        @field:Json(name = "name")
        val name: String,

        @field:Json(name = "photo")
        val photo: String,

        @field:Json(name = "contact")
        val contact: List<Contact>,

        @field:Json(name = "week")
        val week: Int,

        @field:Json(name = "loc")
        val loc: List<Location>
    ) {
        fun toAgendaWithUser(): AgendaWithUser {
            val agenda = Agenda(userCreatorId = id, week = week, loc = loc)
            val user = User(id = id, name = name, photo = photo, contact = contact)
            return AgendaWithUser(agenda, user)
        }

        companion object {
            fun from(agendaWithUser: AgendaWithUser): AgendaWithUserJson {
                return AgendaWithUserJson(
                    id = agendaWithUser.user.id,
                    name = agendaWithUser.user.name,
                    photo = agendaWithUser.user.photo,
                    contact = agendaWithUser.user.contact,
                    week = agendaWithUser.agenda.week,
                    loc = agendaWithUser.agenda.loc
                )
            }
        }
    }

    companion object {
        @FromJson
        fun parseAgendaWithUser(json: String): AgendaWithUser? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<AgendaWithUserJson> = moshi.adapter(AgendaWithUserJson::class.java)
            return adapter.fromJson(json)?.toAgendaWithUser()
        }

        @ToJson
        fun agendaWithUserToJson(agendaWithUser: AgendaWithUser): String {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<AgendaWithUserJson> = moshi.adapter(AgendaWithUserJson::class.java)
            return adapter.toJson(AgendaWithUserJson.from(agendaWithUser))
        }
    }

}