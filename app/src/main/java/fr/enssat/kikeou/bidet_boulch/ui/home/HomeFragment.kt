package fr.enssat.kikeou.bidet_boulch.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import fr.enssat.kikeou.bidet_boulch.MainActivity
import fr.enssat.kikeou.bidet_boulch.databinding.FragmentHomeBinding
import fr.enssat.kikeou.bidet_boulch.managers.UserManager

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)

        binding = FragmentHomeBinding.inflate(inflater)

        // Set cards click listeners
        binding.profileCard.setOnClickListener { redirectToProfile() }
        binding.contactCard.setOnClickListener { redirectToContact() }
        binding.qrCodeCard.setOnClickListener { redirectToQRCodeScanner() }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (requireActivity() as MainActivity).supportActionBar?.show()
    }

    private fun redirectToProfile() {
        UserManager().getUserId(requireContext())?.let {
            val action = HomeFragmentDirections.actionNavigationHomeToNavigationUser(it)
            Navigation.findNavController(requireView()).navigate(action)
        }
    }

    private fun redirectToContact() {
        val action = HomeFragmentDirections.actionNavigationHomeToNavigationContacts()
        Navigation.findNavController(requireView()).navigate(action)
    }

    private fun redirectToQRCodeScanner() {
        val action = HomeFragmentDirections.actionNavigationHomeToNavigationQrcode()
        Navigation.findNavController(requireView()).navigate(action)
    }

}