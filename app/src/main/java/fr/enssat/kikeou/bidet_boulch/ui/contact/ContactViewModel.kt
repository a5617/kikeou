package fr.enssat.kikeou.bidet_boulch.ui.contact

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.database.repositories.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class ContactViewModel(private val repository: UserRepository) : ViewModel() {
    private val scope = CoroutineScope(SupervisorJob())

    // Update UI when users data changes
    val contactUsers = repository.notApplicationOwnerUsers.asLiveData()

    fun deleteUser(user: User) = scope.launch {
        repository.delete(user)
    }

}