package fr.enssat.kikeou.bidet_boulch.ui.user_agenda

import androidx.lifecycle.*
import androidx.lifecycle.asLiveData
import fr.enssat.kikeou.bidet_boulch.database.entities.*
import fr.enssat.kikeou.bidet_boulch.database.repositories.AgendaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.util.*

class UserAgendaViewModel(private val repository: AgendaRepository) : ViewModel() {

    private val scope = CoroutineScope(SupervisorJob())

    var currentUser: User? = null
    var currentUserAgendas: List<Agenda>? = null
    var currentAgenda: Agenda? = null
    var currentWeek: MutableLiveData<Int> =
        MutableLiveData(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR))

    /**
     * Increase the current displayed week index.
     */
    fun increaseWeek() {
        if (currentWeek.value == 52) {
            currentWeek.value = 1
        } else {
            currentWeek.value = currentWeek.value?.plus(1)
        }
    }

    /**
     * Increase the current displayed week to the first one which contains locations.
     */
    fun increaseWeekToFirstFilled() {
        val nextAgenda = currentUserAgendas?.find { agenda -> agenda.week > currentWeek.value!! }
        nextAgenda?.let { currentWeek.value = it.week }
    }

    /**
     * Decrease the current displayed week index.
     */
    fun decreaseWeek() {
        if (currentWeek.value == 1) {
            currentWeek.value = 52
        } else {
            currentWeek.value = currentWeek.value?.minus(1)
        }
    }

    /**
     * Decrease the current displayed week to the first one which contains locations.
     */
    fun decreaseWeekToFirstFilled() {
        val prevAgenda = currentUserAgendas?.asReversed()?.find { agenda -> agenda.week < currentWeek.value!! }
        prevAgenda?.let { currentWeek.value = it.week }
    }

    fun isThereFilledAgendaNext(): Boolean {
        val nextAgenda = currentUserAgendas?.find { agenda -> agenda.week > currentWeek.value!! }
        return nextAgenda != null
    }

    fun isThereFilledAgendaPrev(): Boolean {
        val prevAgenda = currentUserAgendas?.find { agenda -> agenda.week < currentWeek.value!! }
        return prevAgenda != null
    }

    fun getAgendas(userId: String): LiveData<List<Agenda>> =
        repository.getAgendaByUserId(userId).asLiveData()

    fun getCurrentAgendaWithUser(): LiveData<AgendaWithUser>? {
        return currentAgenda?.let {
            repository.getAgendaWithUserByAgendaId(it.getId()).asLiveData()
        }
    }

    fun insertLocationInCurrentAgenda(loc: Location) = scope.launch {
        if (currentAgenda != null) {
            currentAgenda!!.loc += loc
            repository.update(currentAgenda!!)
        } else {
            val newLocList = listOf(loc)
            val newAgenda = Agenda(currentUser!!.id, currentWeek.value!!, newLocList)
            repository.insert(newAgenda)
        }
    }

    fun updateCurrentAgenda() = scope.launch {
        currentAgenda?.let { repository.update(it) }
    }

    fun deleteLocationInCurrentAgenda(location: Location) = scope.launch {
        currentAgenda?.let {
            it.loc -= location
            repository.update(it)
        }
    }
}