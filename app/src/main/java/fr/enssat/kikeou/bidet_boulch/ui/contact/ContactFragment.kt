package fr.enssat.kikeou.bidet_boulch.ui.contact

import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import fr.enssat.kikeou.bidet_boulch.R
import fr.enssat.kikeou.bidet_boulch.database.KikeouDatabase
import fr.enssat.kikeou.bidet_boulch.database.repositories.UserRepository
import fr.enssat.kikeou.bidet_boulch.databinding.FragmentContactBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import android.view.MenuInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.appcompat.content.res.AppCompatResources

class ContactFragment : Fragment(), SearchView.OnQueryTextListener {
    private lateinit var binding: FragmentContactBinding
    private lateinit var viewModel: ContactViewModel
    private lateinit var contactListAdapter: ContactListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)

        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouDatabase.getDatabase(requireContext(), scope)
        val repository = UserRepository(database.userDao())

        (activity as AppCompatActivity).supportActionBar?.title = resources.getString(R.string.title_contacts)
        setHasOptionsMenu(true)

        // Inflate view
        binding = FragmentContactBinding.inflate(inflater, container, false)

        // Init ViewModel
        viewModel = ViewModelProvider(
            this,
            ContactViewModelFactory(repository)
        ).get(ContactViewModel::class.java)

        // Init View
        contactListAdapter = ContactListAdapter()
        binding.recyclerview.adapter = contactListAdapter
        binding.recyclerview.layoutManager = LinearLayoutManager(requireContext())

        viewModel.contactUsers.observe(viewLifecycleOwner) { users ->
            contactListAdapter.submitList(users)
        }

        contactListAdapter.setOnItemDeleteButtonClickListener {
            viewModel.deleteUser(it)
        }

        binding.addContactBtn.setOnClickListener {
            redirectToQRCodeScanner()
        }

        toggleContactDeletionOff()

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.search_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val searchItem = menu.findItem(R.id.menu_search)
        val searchView = searchItem.actionView as SearchView
        searchView.apply {
            isSubmitButtonEnabled = true
            searchView.setOnQueryTextListener(this@ContactFragment)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (!isVisible) { return true }
        contactListAdapter.filter.filter(newText)
        return true
    }

    private fun toggleContactDeletionOff() {
        binding.removeContactBtn.setOnClickListener {
            val typedValue = TypedValue()
            requireContext().theme.resolveAttribute(R.attr.colorError, typedValue, true)

            binding.removeContactBtn.backgroundTintList =
                AppCompatResources.getColorStateList(requireContext(), typedValue.resourceId)

            contactListAdapter.areItemsDeletable = true

            toggleContactDeletionOn()
        }
    }

    private fun toggleContactDeletionOn() {
        binding.removeContactBtn.setOnClickListener {
            val typedValue = TypedValue()
            requireContext().theme.resolveAttribute(R.attr.colorPrimary, typedValue, true)

            binding.removeContactBtn.backgroundTintList =
                AppCompatResources.getColorStateList(requireContext(), typedValue.resourceId)

            contactListAdapter.areItemsDeletable = false

            toggleContactDeletionOff()
        }
    }

    private fun redirectToQRCodeScanner() {
        val action = ContactFragmentDirections.actionNavigationContactsToNavigationQrcode()
        Navigation.findNavController(requireView()).navigate(action)
    }
}