package fr.enssat.kikeou.bidet_boulch.managers

import android.content.Context
import java.util.*

class UserManager {
    companion object {
        lateinit var SP_NAME: String
        lateinit var USER_ID: String
    }

    init {
        SP_NAME = "user"
        USER_ID = "user_id"
    }

    fun initUserId(context: Context) {
        if (getUserId(context) == null) {
            setUserId(context, UUID.randomUUID().toString())
        }
    }

    fun getUserId(context: Context): String? {
        val sharedPref = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        return sharedPref.getString(USER_ID, null)
    }

    private fun setUserId(context: Context, userId: String) {
        val sharedPref = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(USER_ID, userId)
        editor.apply()
    }
}