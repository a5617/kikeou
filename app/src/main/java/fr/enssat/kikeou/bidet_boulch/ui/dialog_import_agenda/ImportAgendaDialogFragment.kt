package fr.enssat.kikeou.bidet_boulch.ui.dialog_import_agenda

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import fr.enssat.kikeou.bidet_boulch.R
import fr.enssat.kikeou.bidet_boulch.database.entities.AgendaWithUser
import fr.enssat.kikeou.bidet_boulch.databinding.DialogImportAgendaBinding
import fr.enssat.kikeou.bidet_boulch.ui.user_agenda.UserAgendaLocationListAdapter

class ImportAgendaDialogFragment(
    val agendaWithUser: AgendaWithUser
) : DialogFragment() {

    private lateinit var binding: DialogImportAgendaBinding

    private var onDismissListener: (() -> Unit)? = null
    private var onSaveButtonClickListener: (() -> Unit)? = null

    fun setOnDismissListener(listener: () -> Unit) {
        this.onDismissListener = listener
    }

    fun setOnSaveButtonClickListener(listener: () -> Unit) {
        this.onSaveButtonClickListener = listener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = DialogImportAgendaBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Bind the toolbar
        binding.toolbar.title = resources.getString(R.string.dialog_import_agenda_title)
        binding.toolbar.inflateMenu(R.menu.dialog_import_agenda)
        binding.toolbar.setNavigationOnClickListener {
            dismiss()
        }
        binding.toolbar.setOnMenuItemClickListener {
            onSaveButtonClickListener?.invoke()
            true
        }

        // Bind the user fields
        binding.idField.editText?.setText(agendaWithUser.user.id)
        binding.nameField.editText?.setText(agendaWithUser.user.name)

        // Bind the agenda fields
        binding.weekField.editText?.setText(agendaWithUser.agenda.week.toString())

        val agendaLocationListAdapter = UserAgendaLocationListAdapter()
            .also {
                it.submitList(agendaWithUser.agenda.loc)
            }
        binding.locationRecyclerview.adapter = agendaLocationListAdapter
        binding.locationRecyclerview.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onStart() {
        super.onStart()

        dialog?.let {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            it.window?.setLayout(width, height)
            it.window?.setWindowAnimations(R.style.AppTheme_Slide)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismissListener?.invoke()
    }

    companion object {
        const val TAG = "import_agenda_dialog"

        fun display(
            fragmentManager: FragmentManager,
            agendaWithUser: AgendaWithUser
        ): ImportAgendaDialogFragment {
            val importAgendaDialog = ImportAgendaDialogFragment(agendaWithUser)
            importAgendaDialog.show(fragmentManager, TAG)
            return importAgendaDialog
        }
    }
}