package fr.enssat.kikeou.bidet_boulch.ui.user_profile

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.bidet_boulch.databinding.ListItemUserContactBinding

class UserProfileContactViewHolder(
    var binding: ListItemUserContactBinding
) : RecyclerView.ViewHolder(binding.root) {

    private var onEditButtonClickedListener: ((Int) -> Unit)? = null
    private var onDeleteButtonClickedListener: ((Int) -> Unit)? = null

    init {
        binding.userContactEditBtn.setOnClickListener {
            onEditButtonClickedListener?.invoke(adapterPosition)
        }

        binding.userContactDeleteBtn.setOnClickListener {
            onDeleteButtonClickedListener?.invoke(adapterPosition)
        }
    }

    fun bind(key: String?, value: String?, enable: Boolean = false) {
        binding.userContactKey.text = key
        binding.userContactValue.text = value

        binding.userContactEditBtn.visibility = if (enable) View.VISIBLE else View.GONE
        binding.userContactDeleteBtn.visibility = if (enable) View.VISIBLE else View.GONE
    }

    fun setOnEditButtonClicked(listener: (itemIndex: Int) -> Unit) {
        this.onEditButtonClickedListener = listener
    }

    fun setOnDeleteButtonClicked(listener: (itemIndex: Int) -> Unit) {
        this.onDeleteButtonClickedListener = listener
    }

    companion object {
        fun create(binding: ListItemUserContactBinding): UserProfileContactViewHolder {
            return UserProfileContactViewHolder(binding)
        }
    }
}