package fr.enssat.kikeou.bidet_boulch.ui.camera

import android.os.Bundle
import fr.enssat.kikeou.bidet_boulch.R
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.google.common.util.concurrent.ListenableFuture
import fr.enssat.kikeou.bidet_boulch.database.KikeouDatabase
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.database.repositories.AgendaRepository
import fr.enssat.kikeou.bidet_boulch.databinding.FragmentCameraBinding
import fr.enssat.kikeou.bidet_boulch.json.AgendaWithUserJsonAdapter
import fr.enssat.kikeou.bidet_boulch.ui.dialog_import_agenda.ImportAgendaDialogFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class CameraFragment : Fragment() {

    private val REQUEST_CODE = 123456

    private lateinit var binding: FragmentCameraBinding
    private lateinit var viewModel: CameraViewModel

    private lateinit var qrCodeBoxView: QrCodeBoxView
    private lateinit var imageAnalyzer: QrCodeAnalyzer
    private lateinit var imageAnalysis: ImageAnalysis
    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)

        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouDatabase.getDatabase(requireContext(), scope)
        val repository = AgendaRepository(database.agendaDao())

        // Set header bar title
        (activity as AppCompatActivity).supportActionBar?.title =
            resources.getString(R.string.title_qrcode)

        // Inflate View
        binding = FragmentCameraBinding.inflate(inflater, container, false)

        qrCodeBoxView = QrCodeBoxView(requireContext())
        binding.root.addView(
            qrCodeBoxView,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )

        // Init the ViewModel
        viewModel = ViewModelProvider(
            this,
            CameraViewModelFactory(repository)
        ).get(CameraViewModel::class.java)

        return binding.root
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startCamera()
                } else {
                    Toast.makeText(
                        requireContext(),
                        this.getString(R.string.permissionToGrant),
                        Toast.LENGTH_LONG
                    ).show()
                }
                return
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startCamera()
        } else {
            val permissions = arrayOf(android.Manifest.permission.CAMERA)
            ActivityCompat.requestPermissions(requireActivity(), permissions, REQUEST_CODE)
        }
    }

    //Only the original thread that created a view can touch its views.
    private fun startCamera() = binding.previewView.post {
        // Future do not block get() is used to get the instance of the future when available
        cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener(
            {
                val cameraProvider = cameraProviderFuture.get()

                // Set up the view finder use case to display camera preview
                val preview = Preview.Builder()
                    .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                    .build()

                // Set up analyzer
                imageAnalyzer = QrCodeAnalyzer(
                    qrCodeBoxView,
                    binding.previewView.width.toFloat(),
                    binding.previewView.height.toFloat()
                )
                imageAnalyzer.setOnQRCodeDetectedListener {
                    openImportAgendaDialog(it.rawValue)
                }

                // Set up the image analysis use case which will process frames in real time
                imageAnalysis = ImageAnalysis.Builder()
                    .setTargetResolution(Size(1080, 2340))
                    .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                    .build()

                // Start analysis (setting analyzer)
                startImageAnalysis()

                // Create a new camera selector each time, enforcing lens facing
                val cameraSelector =
                    CameraSelector
                        .Builder()
                        .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                        .build()

                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    this as LifecycleOwner, cameraSelector, preview, imageAnalysis
                )

                // Use the camera object to link our preview use case with the view
                preview.setSurfaceProvider(binding.previewView.surfaceProvider)
            },
            ContextCompat.getMainExecutor(requireContext())
        )
    }

    private fun openImportAgendaDialog(json: String?) {
        json?.let { agendaWithUserJson ->
            val agendaWithUser = AgendaWithUserJsonAdapter.parseAgendaWithUser(agendaWithUserJson)
            agendaWithUser?.let { agendaWUser ->
                stopImageAnalysis()
                val dialog =
                    ImportAgendaDialogFragment.display(parentFragmentManager, agendaWUser)
                dialog.setOnDismissListener { startImageAnalysis() }
                dialog.setOnSaveButtonClickListener {
                    viewModel.saveUserWithAgenda(agendaWUser).invokeOnCompletion {
                        dialog.dismiss()
                    }
                    redirectToUser(agendaWUser.user)
                }
            }
        }
    }

    private fun redirectToUser(user: User) {
        val action = CameraFragmentDirections.actionNavigationQrcodeToNavigationUser(user.id)
        Navigation.findNavController(requireView()).navigate(action)
    }

    private fun startImageAnalysis() {
        imageAnalysis.setAnalyzer(
            ContextCompat.getMainExecutor(requireContext()),
            imageAnalyzer
        )
    }

    private fun stopImageAnalysis() {
        imageAnalysis.clearAnalyzer()
    }
}