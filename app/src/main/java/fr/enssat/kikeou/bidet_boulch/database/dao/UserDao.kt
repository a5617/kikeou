package fr.enssat.kikeou.bidet_boulch.database.dao

import androidx.room.*
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {
    @Delete
    fun delete(user: User)

    @Insert
    fun insert(user: User)

    @Update
    suspend fun update(vararg users: User)

    @Query("DELETE FROM user_table")
    fun deleteAll()

    @Query("SELECT * FROM user_table WHERE isApplicationOwner = :getApplicationOwner ORDER BY name ASC")
    fun getAlphabetizedUsers(getApplicationOwner: Boolean = true): Flow<List<User>>

    @Query("SELECT * FROM user_table WHERE id = :id")
    fun getUserById(id: String): Flow<User?>

    @Query("SELECT id FROM user_table WHERE isApplicationOwner = :isApplicationOwner")
    fun getApplicationOwnerId(isApplicationOwner: Boolean = true): String?
}