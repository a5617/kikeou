package fr.enssat.kikeou.bidet_boulch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.enssat.kikeou.bidet_boulch.databinding.ActivityMainBinding
import fr.enssat.kikeou.bidet_boulch.managers.UserManager

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Init user
        UserManager().initUserId(applicationContext)

        val navController = findNavController(R.id.nav_host_fragment)

        // Initialize the bottom navigation view and create bottom navigation view object
        val bottomNavigationView: BottomNavigationView = binding.bottomNavigationView
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_user -> {
                    val userId = UserManager().getUserId(applicationContext)
                    val args = bundleOf("userId" to userId)

                    navController.navigate(item.itemId, args)
                    true
                }
                else -> {
                    navController.navigate(item.itemId)
                    true
                }
            }
        }
    }
}