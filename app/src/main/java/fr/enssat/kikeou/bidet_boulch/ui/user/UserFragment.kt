package fr.enssat.kikeou.bidet_boulch.ui.user

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator
import fr.enssat.kikeou.bidet_boulch.MainActivity
import fr.enssat.kikeou.bidet_boulch.R
import fr.enssat.kikeou.bidet_boulch.database.KikeouDatabase
import fr.enssat.kikeou.bidet_boulch.database.entities.User
import fr.enssat.kikeou.bidet_boulch.database.repositories.UserRepository
import fr.enssat.kikeou.bidet_boulch.databinding.FragmentUserBinding
import fr.enssat.kikeou.bidet_boulch.ui.user_agenda.UserAgendaFragment
import fr.enssat.kikeou.bidet_boulch.ui.user_profile.UserProfileFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class UserFragment : Fragment() {

    private lateinit var binding: FragmentUserBinding
    lateinit var viewModel: UserViewModel

    private lateinit var viewPager: ViewPager2
    private lateinit var viewPagerAdapter: ViewPagerAdapter

    private val args by navArgs<UserFragmentArgs>()
    val userId by lazy { args.userId }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)

        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouDatabase.getDatabase(requireContext(), scope)
        val repository = UserRepository(database.userDao())

        binding = FragmentUserBinding.inflate(inflater, container, false)

        // Initializing the ViewModel
        viewModel = ViewModelProvider(
            this,
            UserViewModelFactory(userId, repository)
        ).get(UserViewModel::class.java)

        viewModel.user.observe(viewLifecycleOwner) { user ->
            if (user != null) {
                updateUserNameText(user.name)
                updateUserIdText(user.id)
                updateUserPhoto(user.photo)
            } else {
                updateUserNameText("Undefined")
                updateUserIdText(this.userId)

                // Try adding user in database
                val appOwnerUser = User()
                appOwnerUser.id = this.userId
                appOwnerUser.setIsApplicationOwner(true)
                viewModel.insertUser(appOwnerUser)
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initializing the ViewPagerAdapter
        viewPagerAdapter = ViewPagerAdapter(childFragmentManager, lifecycle)
        viewPagerAdapter.addFragment(
            UserProfileFragment(),
            resources.getString(R.string.user_tab_info_title),
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_baseline_account_box_24
            )!!
        )
        viewPagerAdapter.addFragment(
            UserAgendaFragment(),
            resources.getString(R.string.user_tab_agenda_title),
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_baseline_calendar_today_24
            )!!
        )

        // Initializing the ViewPager
        viewPager = binding.viewPager
        viewPager.adapter = viewPagerAdapter

        val tabLayout = binding.tabs
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = viewPagerAdapter.getPageTitle(position)
            tab.icon = viewPagerAdapter.getPageIcon(position)
        }.attach()
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (requireActivity() as MainActivity).supportActionBar?.show()
    }

    private fun updateUserIdText(userId: String) {
        binding.userId.text =
            resources.getString(R.string.user_id, userId)
    }

    private fun updateUserNameText(userName: String) {
        binding.userName.text = userName
    }

    private fun updateUserPhoto(url: String) {
        Glide.with(requireContext())
            .load(url)
            .placeholder(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_baseline_person_24
                )
            )
            .into(binding.userPhoto)
    }

    private class ViewPagerAdapter(supportFragmentManager: FragmentManager, lifecycle: Lifecycle) :
        FragmentStateAdapter(supportFragmentManager, lifecycle) {

        // declare arrayList to contain fragments and its title
        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val fragmentTitleList: MutableList<String> = ArrayList()
        private val fragmentIconList: MutableList<Drawable> = ArrayList()

        override fun createFragment(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getItemCount(): Int {
            // return the number of tabs
            return fragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String, icon: Drawable) {
            // add each fragment and its title to the array list
            fragmentList.add(fragment)
            fragmentTitleList.add(title)
            fragmentIconList.add(icon)
        }

        fun getPageTitle(position: Int): CharSequence {
            return fragmentTitleList[position]
        }

        fun getPageIcon(position: Int): Drawable {
            return fragmentIconList[position]
        }
    }
}