package fr.enssat.kikeou.bidet_boulch.ui.user_agenda

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.enssat.kikeou.bidet_boulch.database.repositories.AgendaRepository

class UserAgendaViewModelFactory(private val repository: AgendaRepository) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    //factory created to pass repository to view model...
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return UserAgendaViewModel(repository) as T
    }
}