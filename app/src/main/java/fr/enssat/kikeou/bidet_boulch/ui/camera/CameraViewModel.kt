package fr.enssat.kikeou.bidet_boulch.ui.camera

import androidx.lifecycle.ViewModel
import fr.enssat.kikeou.bidet_boulch.database.entities.AgendaWithUser
import fr.enssat.kikeou.bidet_boulch.database.repositories.AgendaRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class CameraViewModel(private val repository: AgendaRepository) : ViewModel() {

    private val scope = CoroutineScope(SupervisorJob())

    fun saveUserWithAgenda(agendaWithUser: AgendaWithUser) = scope.launch {
        repository.saveAgendaWithUser(agendaWithUser)
    }
}