package fr.enssat.kikeou.bidet_boulch.ui.user_profile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.enssat.kikeou.bidet_boulch.database.entities.Contact
import fr.enssat.kikeou.bidet_boulch.databinding.ListItemUserContactBinding

class UserProfileContactListAdapter :
    ListAdapter<Contact, UserProfileContactViewHolder>(ContactDiff()) {

    var areValueEditable: Boolean = false

    private var onItemDeleteButtonClickListener: ((item: Contact) -> Unit)? = null
    private var onItemEditButtonClickListener: ((item: Contact) -> Unit)? = null

    fun setOnItemDeleteButtonClickListener(listener: (item: Contact) -> Unit) {
        this.onItemDeleteButtonClickListener = listener
    }

    fun setOnItemEditButtonClickListener(listener: (item: Contact) -> Unit) {
        this.onItemEditButtonClickListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UserProfileContactViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemUserContactBinding.inflate(inflater, parent, false)
        val viewHolder = UserProfileContactViewHolder.create(binding)

        viewHolder.setOnEditButtonClicked { onItemEditButtonClickListener?.invoke(getItem(it)) }
        viewHolder.setOnDeleteButtonClicked { onItemDeleteButtonClickListener?.invoke(getItem(it)) }

        return viewHolder
    }

    override fun onBindViewHolder(viewHolder: UserProfileContactViewHolder, position: Int) {
        val current: Contact = getItem(position)
        val key = current.key
        val value = current.value
        viewHolder.bind(key, value, areValueEditable)
    }

    internal class ContactDiff : DiffUtil.ItemCallback<Contact>() {
        override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.key == newItem.key
        }
    }
}